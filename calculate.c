/* Josh Feener's chess program

 */

#include "defs.h"

_Bool computerMove(BOARD *the_board, struct in_move *comp_move, int side_to_move){
   srand(time(NULL));
   
   U64 allPiecesbb = FindOccupiedSquares(the_board);
   U64 friendlyPieces = the_board->bitboard[side_to_move];

   char piece_type = randomPiece();
   int piece_num = piece_correct(piece_type, side_to_move);

//first find which specific piece of the piece type you want to move
   U64 current_sq = the_board->bitboard[piece_num];    //if there is no piece of its kind
   if(number_of_pieces(current_sq) == 0){ return(0); } //to prevent division by zero

   int sq_nums_from[64];
   for(int i=0; i<64; i++) { sq_nums_from[i] = -1; }
   calculateSqs(current_sq, sq_nums_from);

   int rf = rand() % number_of_pieces(current_sq);
   int sq_from = sq_nums_from[rf];

   int filef = findFile(sq_from);
   int rankf = findRank(sq_from);

   int enemy_side = switch_turn(side_to_move); 
   U64 enemyPiecesbb = the_board->bitboard[enemy_side];

//then find where you want to move the piece to
   U64 move_to = calculate(the_board, piece_type, sq_from, allPiecesbb, friendlyPieces, enemyPiecesbb, side_to_move);
   if(number_of_pieces(move_to) == 0){ return(0); } //to prevent division by zero

   int sq_nums_to[64];
   for(int i=0; i<64; i++) { sq_nums_to[i] = -1; }
   generateMoves(sq_nums_to, piece_type, move_to);//need to generate places piece can move to  
//   printGenMoves(sq_nums_to, piece_type);

   int rt = rand() % number_of_pieces(move_to);
   int sq_to = sq_nums_to[rt];

   int filet = findFile(sq_to);
   int rankt = findRank(sq_to);

   comp_move->piece_type = piece_type;
   comp_move->piece_num = piece_num;
   comp_move->sq_from = sq_from;
   comp_move->sq_to = sq_to;
   comp_move->filef = filef;
   comp_move->rankf = rankf;
   comp_move->filet = filet;
   comp_move->rankt = rankt;
   return(1);   //computer moved successfully
}

void updateBoard(BOARD *the_board, struct in_move *current_move, int side_to_move){
   U64 previous_bb = the_board->bitboard[current_move->piece_num];
   U64 sides_pieces = the_board->bitboard[side_to_move];
   
   U64 move_from = calculatebb(current_move->sq_from);  //delete where the piece previously was on the bitboard
   the_board->bitboard[current_move->piece_num] = ~move_from & previous_bb;  
   the_board->bitboard[side_to_move] = ~move_from & sides_pieces;

   U64 move_to = calculatebb(current_move->sq_to);      //update bitboards with new move
   the_board->bitboard[current_move->piece_num] = move_to | previous_bb;
   the_board->bitboard[side_to_move] = move_to | sides_pieces;

   //if move is a capture, update the bitboard of the enemy piece that was captured
   int enemy_side = switch_turn(side_to_move);
   for(int piece_num = 2 + enemy_side; piece_num < 12; piece_num = piece_num+2){
      if((move_to & the_board->bitboard[piece_num]) != 0){
         the_board->bitboard[piece_num] = ~move_to & the_board->bitboard[piece_num];
         the_board->bitboard[enemy_side] = ~move_to & the_board->bitboard[enemy_side];
      }
   }
}

_Bool computerKingMove(BOARD *the_board, struct in_move *comp_move, int side_to_move){
   srand(time(NULL)); //for generating a random move
   U64 allPiecesbb = FindOccupiedSquares(the_board);
   U64 friendlyPieces = the_board->bitboard[side_to_move];
   int enemy_side = switch_turn(side_to_move);
   U64 enemyPiecesbb = the_board->bitboard[enemy_side];

   char piece_type = 'K';
   int piece_num = piece_correct(piece_type, side_to_move);
   int sq_from = calculateSq(the_board->bitboard[wK + side_to_move]);
   int filef = findFile(sq_from);
   int rankf = findRank(sq_from);

   U64 move_to = calculate(the_board, piece_type, sq_from, allPiecesbb, friendlyPieces, enemyPiecesbb, side_to_move);
   if(number_of_pieces(move_to) == 0){ return(0); } //to prevent division by zero. for when king can't move

   int sq_nums_to[64];
   for(int i=0; i<64; i++) { sq_nums_to[i] = -1; }
   generateMoves(sq_nums_to, piece_type, move_to);//need to generate places piece can move to
//   printGenMoves(sq_nums_to, piece_type);

   int rt = rand() % number_of_pieces(move_to);
   int sq_to = sq_nums_to[rt];

   int filet = findFile(sq_to);
   int rankt = findRank(sq_to);

   comp_move->piece_type = piece_type;
   comp_move->piece_num = piece_num;
   comp_move->sq_from = sq_from;
   comp_move->sq_to = sq_to;
   comp_move->filef = filef;
   comp_move->rankf = rankf;
   comp_move->filet = filet;
   comp_move->rankt = rankt;
   return(1);   //computer moved successfully

}

U64 checkedKingMove(BOARD *the_board, int side_to_move){
   U64 allPieces = FindOccupiedSquares(the_board);
   U64 friendlyPieces = the_board->bitboard[side_to_move];

   int enemy_side = switch_turn(side_to_move);
   U64 enemyPiecesbb = the_board->bitboard[enemy_side];

   //first put all the squares the enemy pieces are attacking into a bitboard
   U64 allEnemyAttacks = 0;
   for(int piece_num = 2 + enemy_side; piece_num < 12; piece_num = piece_num+2){
      char piece_type = piece_type_correct(piece_num);

      U64 current_sq = the_board->bitboard[piece_num];    //if there is no piece of its kind
      if(number_of_pieces(current_sq) != 0){              //to prevent division by zero
         int sq_nums_from[64];
         for(int i=0; i<64; i++) { sq_nums_from[i] = -1; }
         calculateSqs(current_sq, sq_nums_from);

         int i = 0, sq_from = sq_nums_from[i];
         while(sq_from != -1){
            U64 piece_attacks = calculate(the_board, piece_type, sq_from, allPieces, friendlyPieces, enemyPiecesbb, side_to_move);
            allEnemyAttacks |= piece_attacks;
            i++;
            sq_from = sq_nums_from[i];
         }
      }
   }
/*   printf("allEnemyAttacks\n");
   PrintBitBoard(allEnemyAttacks);
*/
   //then for every king move, AND it with the allEnemyAttacks bb to see if it if legal
   U64 move_from = calculateSq(the_board->bitboard[wK + side_to_move]);
   U64 possible_moves = kingMove(move_from, friendlyPieces);
/*   printf("move_from\n");
   PrintBitBoard(move_from);
   printf("friendlyPieces\n");
   PrintBitBoard(friendlyPieces);
   printf("possible_moves\n");
   PrintBitBoard(possible_moves);*/
   U64 moves_can_make = 0;

   if((number_of_pieces(possible_moves)) != 0){  //if the king can actually move...
      int kings_moves[64];
      for(int i=0; i<64; i++) { kings_moves[i] = -1; }
      calculateSqs(possible_moves, kings_moves);

      int i = 0, one_king_move = kings_moves[i];
      while(one_king_move != -1){    //*** for each king move, find if a piece attacks that square ***
         U64 o_k_m = (1ULL << one_king_move);
         if((o_k_m & allEnemyAttacks) == 0) { moves_can_make |= o_k_m; }
         i++;
         one_king_move = kings_moves[i]; 
      }
   }
/*   printf("moves_can_make\n");
   PrintBitBoard(moves_can_make);*/

   return(moves_can_make);
}

U64 calculate(BOARD *the_board, char piece_type, int sq_from, U64 allPiecesbb, U64 friendlyPiecesbb, U64 enemyPiecesbb, int side_to_move){
   U64 moves_can_make = 0, move_from = 0, move_to = 0; //have to initialize to 0 or else unpredictable results!
   switch(piece_type){
      case 'K' :
         moves_can_make = checkedKingMove(the_board, side_to_move);
/*         move_from = calculatebb(sq_from);
//         move_to = calculatebb(current_move->sq_to);
         moves_can_make = kingMove(move_from, friendlyPiecesbb);*/
         break;
      case 'Q' :
//         move_to = calculatebb(current_move->sq_to);
         moves_can_make = queenMove(sq_from, allPiecesbb, friendlyPiecesbb);
         break;
      case 'R' :
//         move_to = calculatebb(current_move->sq_to);
         moves_can_make = rookMove(sq_from, allPiecesbb, friendlyPiecesbb);
         break;
      case 'B' :
//         move_to = calculatebb(current_move->sq_to);
         moves_can_make = bishopMove(sq_from, allPiecesbb, friendlyPiecesbb);
         break;
      case 'N' :
         move_from = calculatebb(sq_from);
//         move_to = calculatebb(current_move->sq_to);
         moves_can_make = knightMove(move_from, friendlyPiecesbb);
         break;
      default:
         move_from = calculatebb(sq_from);
//         move_to = calculatebb(current_move->sq_to);
         moves_can_make = pawnMove(move_from, sq_from, allPiecesbb, enemyPiecesbb, side_to_move);
   }
   return(moves_can_make);
}

_Bool isValidMove(BOARD *the_board, struct in_move *current_move, U64 where_piece, U64 allPiecesbb, U64 friendlyPiecesbb, U64 enemyPiecesbb, int side_to_move){
   U64 moves_can_make = 0, move_from = 0, move_to = 0; //have to initialize to 0 or else unpredictable results!
   //depending on piece type, calculate the move
   switch(current_move->piece_type){
      case 'K' :
         move_from = calculatebb(current_move->sq_from);
         move_to = calculatebb(current_move->sq_to);
         moves_can_make = checkedKingMove(the_board, side_to_move);
//         moves_can_make = kingMove(move_from, friendlyPiecesbb);
         if((where_piece & move_from) == 0){ return(0); }
         if((moves_can_make & move_to) == 0){ return(0); }
         return(1);
         break;
      case 'Q' :
         move_from = calculatebb(current_move->sq_from);
         move_to = calculatebb(current_move->sq_to);
         moves_can_make = queenMove(current_move->sq_from, allPiecesbb, friendlyPiecesbb);
         if((where_piece & move_from) == 0){ return(0); }
         if((moves_can_make & move_to) == 0){ return(0); }
         return(1);
         break;
      case 'R' :
         move_from = calculatebb(current_move->sq_from);
         move_to = calculatebb(current_move->sq_to);
         moves_can_make = rookMove(current_move->sq_from, allPiecesbb, friendlyPiecesbb);
         if((where_piece & move_from) == 0){ return(0); }
         if((moves_can_make & move_to) == 0){ return(0); }
         return(1);
         break;
      case 'B' :
         move_from = calculatebb(current_move->sq_from);
         move_to = calculatebb(current_move->sq_to);
         moves_can_make = bishopMove(current_move->sq_from, allPiecesbb, friendlyPiecesbb);
         if((where_piece & move_from) == 0){ return(0); }
         if((moves_can_make & move_to) == 0){ return(0); }
         return(1);
         break;
      case 'N' :
         move_from = calculatebb(current_move->sq_from);
         move_to = calculatebb(current_move->sq_to);
         moves_can_make = knightMove(move_from, friendlyPiecesbb);
         if((where_piece & move_from) == 0){ return(0); }
         if((moves_can_make & move_to) == 0){ return(0); }
         return(1);
         break;
      default  : //is pawn move
         move_from = calculatebb(current_move->sq_from);
         move_to = calculatebb(current_move->sq_to);
         moves_can_make = pawnMove(move_from, current_move->sq_from, allPiecesbb, enemyPiecesbb, side_to_move);
         if((where_piece & move_from) == 0){ return(0); }
         if((moves_can_make & move_to) == 0){ return(0); }
         return(1);
   }
}

/*
         PrintBitBoard(move_from);
         PrintBitBoard(move_to);
         PrintBitBoard(moves_can_make);
*/
U64 calculatebb(int shift){
   U64 move_bb;
//   int shift = current_move->sq_to;
   move_bb = (1ULL << shift);
   return(move_bb);
}

int number_of_pieces(U64 bb){
   int count = 0;
   for(int i=0; i<64; i++){
      if((bb & (1ULL << i)) != 0){ count++; }
   }
   return(count);
}

int calculateSq(U64 bb){
   for(int i=0; i<64; i++){
      if((bb & (1ULL << i)) != 0){ return(i); }
   }
}

void calculateSqs(U64 bb, int *sq_nums){
   int count = 0;
   for(int i=0; i<64; i++){
      if((bb & (1ULL << i)) != 0){ sq_nums[count++] = i; }
   }
}

char piece_type_correct(int piece_num){
   switch(piece_num){
      case 11: return('Q');
         break;
      case 10: return('Q');
         break;
      case 9: return('R');
         break;
      case 8: return('R');
         break;
      case 7: return('B');
         break;
      case 6: return('B');
         break;
      case 5: return('N');
         break;
      case 4: return('N');
         break;
      default: return('P');
   }
}

char randomPiece(){
   int r = rand() % 6;
   switch(r){
      case 0:
         return('K');
         break;
      case 1:
         return('Q');
         break;
      case 2:
         return('R');
         break;
      case 3:
         return('B');
         break;
      case 4:
         return('N');
         break;
      case 5:
         return('P');
         break;
   }
}
