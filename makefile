# Josh Feener's Chess Program
# Makefile

# gcc -g -o b chess.c brq.c move_input.c defs.h

compiler=gcc
switches=-g
headerfi=defs.h
headersp=magic.h magicMovesRook.h magicMovesBishop.h
#tar cvf chess.tar chess.c

play: chess.o move_input.o brq.o pkk.o calculate.o evaluate.o #bitboard_test.o
	$(compiler) $(switches) -o play chess.c move_input.c brq.c pkk.c calculate.c evaluate.c #bitboard_test.c

chess.o: chess.c $(headerfi)
	$(compiler) -c $(switches) chess.c

move_input.o: move_input.c $(headerfi)
	$(compiler) -c $(switches) move_input.c 

brq.o: brq.c $(headerfi)
	$(compiler) -c $(switches) brq.c

pkk.o: pkk.c $(headerfi) $(headersp)
	$(compiler) -c $(switches) pkk.c

calculate.o: calculate.c $(headerfi)
	$(compiler) -c $(switches) calculate.c

evaluate.o: evaluate.c $(headerfi)
	$(compiler) -c $(switches) evaluate.c

#bitboard_test.o: bitboard_test.c $(headerfi)
#	$(compiler) -c $(switches) bitboard_test.c

clean:
	 rm chess.o move_input.o brq.o pkk.o calculate.o evaluate.o #bitboard_test.o
