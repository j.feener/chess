/* Josh Feener's Chess Program
 * Functions for Pawn, King, and Knight moves
 */

#include "defs.h"
#include "magic.h"
#include "magicMovesRook.h"
#include "magicMovesBishop.h"

//are U64 rookbb and U64 bishopbb needed in functions?

U64 rookMove(int sqNum, U64 allPiecesbb, U64 friendlyPiecesbb){
   U64 bbBlockers = allPiecesbb & occupancyMaskRook[sqNum];
   int databaseIndex = (int)((bbBlockers*magicNumberRook[sqNum]) >> magicNumberShiftsRook[sqNum]);
   U64 rooksMovesSquaresBB = magicMovesRook[sqNum][databaseIndex] & ~friendlyPiecesbb;
   return(rooksMovesSquaresBB);
}

U64 bishopMove(int sqNum, U64 allPiecesbb, U64 friendlyPiecesbb){
   U64 bbBlockers = allPiecesbb & occupancyMaskBishop[sqNum];
   int databaseIndex = (int)((bbBlockers*magicNumberBishop[sqNum]) >> magicNumberShiftsBishop[sqNum]);
   U64 bishopMovesSquaresBB = magicMovesBishop[sqNum][databaseIndex] & ~friendlyPiecesbb;
   return(bishopMovesSquaresBB);
}

U64 queenMove(int sqNum, U64 allPiecesbb, U64 friendlyPiecesbb){
   U64 queenMovesSquaresBB = rookMove(sqNum, allPiecesbb, friendlyPiecesbb) |
                             bishopMove(sqNum, allPiecesbb, friendlyPiecesbb);
   return(queenMovesSquaresBB);
}

/*
U64 rookMove(U64 rookbb, int sqNum, U64 allPiecesbb, U64 friendlyPiecesbb){
   U64 bbBlockers = allPiecesbb & occupancyMaskRook[sqNum];
   int databaseIndex = (int)((bbBlockers*magicNumberRook[sqNum]) >> magicNumberShiftsRook[sqNum]);
   U64 rooksMovesSquaresBB = magicMovesRook[sqNum][databaseIndex] & ~friendlyPiecesbb;
   return(rooksMovesSquaresBB);
}

U64 bishopMove(U64 bishopbb, int sqNum, U64 allPiecesbb, U64 friendlyPiecesbb){
   U64 bbBlockers = allPiecesbb & occupancyMaskBishop[sqNum];
   int databaseIndex = (int)((bbBlockers*magicNumberBishop[sqNum]) >> magicNumberShiftsBishop[sqNum]);
   U64 bishopMovesSquaresBB = magicMovesBishop[sqNum][databaseIndex] & ~friendlyPiecesbb;
   return(bishopMovesSquaresBB);
}

U64 queenMove(U64 queenbb, int sqNum, U64 allPiecesbb, U64 friendlyPiecesbb){
   U64 queenMovesSquaresBB = rookMove(queenbb, sqNum, allPiecesbb, friendlyPiecesbb) |
                             bishopMove(queenbb, sqNum, allPiecesbb, friendlyPiecesbb);
   return(queenMovesSquaresBB);
}*/
