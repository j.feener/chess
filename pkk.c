/* Josh Feener's Chess Program
 * Functions for Pawn, King, and Knight moves
 */

#include "defs.h"
#include "pawnAttackArray.h"

U64 pawnMove(U64 pawnSq, int sq_num, U64 allPieces, U64 enemyPiecesbb, int side_to_move){
   if(side_to_move == 0){ //if white to move
      U64 movesForward = wPSingleMove(pawnSq, ~allPieces) | wPDblMove(pawnSq, ~allPieces);
      U64 attacks = paa[side_to_move][sq_num-8];
      U64 allPawnMoves = movesForward | (attacks & enemyPiecesbb);
      return(allPawnMoves); //the two functions in the previous line already checked
   }                        //squares where open
   if(side_to_move == 1){ //black to move
      U64 movesForward = bPSingleMove(pawnSq, ~allPieces) | bPDblMove(pawnSq, ~allPieces);
      U64 attacks = paa[side_to_move][sq_num-8];
      U64 allPawnMoves = movesForward | (attacks & enemyPiecesbb);
      return(allPawnMoves);
   }
}

U64 kingMove(U64 kingSq, U64 friendlyPiecesbb){
   U64 allKingMoves = KMovesAll(kingSq);
   return(allKingMoves & ~friendlyPiecesbb);
}


U64 knightMove(U64 knightSq, U64 friendlyPiecesbb){
   U64 allKnightMoves = NMovesAll(knightSq);
   return(allKnightMoves & ~friendlyPiecesbb);
}


// *** Pawn moves ***

//These find the squares the pawns can move to:
U64 wPSingleMove(U64 wpawns, U64 empty_sqs) {
   return oneN(wpawns) & empty_sqs;
}
 
U64 wPDblMove(U64 wpawns, U64 empty_sqs) {
   const U64 rank4 = 0x00000000FF000000;
   U64 singleMoves = wPSingleMove(wpawns, empty_sqs);
   return oneN(singleMoves) & empty_sqs & rank4;
}
 
U64 bPSingleMove(U64 bpawns, U64 empty_sqs) {
   return oneS(bpawns) & empty_sqs;
}
 
U64 bPDblMove(U64 bpawns, U64 empty_sqs) {
   const U64 rank5 = 0x000000FF00000000;
   U64 singleMoves = bPSingleMove(bpawns, empty_sqs);
   return oneS(singleMoves) & empty_sqs & rank5;
}

//These find the pawns that are able to move:
U64 wPAble2SingleMove(U64 wpawns, U64 empty_sqs) {
   return oneS(empty_sqs) & wpawns;
}
 
U64 wPAble2DblMove(U64 wpawns, U64 empty_sqs) {
   const U64 rank4 = 0x00000000FF000000;
   U64 emptyRank3 = oneS(empty_sqs & rank4) & empty_sqs;
   return wPAble2SingleMove(wpawns, emptyRank3);
}

U64 bPAble2SingleMove(U64 bpawns, U64 empty_sqs) {
   return oneN(empty_sqs) & bpawns;
}

U64 bPAble2DblMove(U64 bpawns, U64 empty_sqs) {
   const U64 rank5 = 0x000000FF00000000;
   U64 emptyRank6 = oneN(empty_sqs & rank5) & empty_sqs;
   return bPAble2SingleMove(bpawns, emptyRank6);
}


// *** King moves ***

U64 KMovesAll(U64 kingSquare){
   return(oneNW(kingSquare) | oneN(kingSquare) | oneNE(kingSquare) | oneE(kingSquare) | 
          oneW(kingSquare) | oneSE(kingSquare) | oneS(kingSquare) | oneSW(kingSquare)); 
}

//    NW   N    NE
//
//    W   Pawn   E
//
//    SW   S    SE

//~0x0101010101010101 is the bitboard for "not A file"
//~0x8080808080808080 is the bitboard for "not H file"
// "-" means empty, "X" means occupied
// Not A File: 
//  ABCDEFGH
//1 -XXXXXXX
//2 -XXXXXXX
//3 -XXXXXXX
//4 -XXXXXXX
//5 -XXXXXXX
//6 -XXXXXXX
//7 -XXXXXXX
//8 -XXXXXXX

//NW (northwest) move one square
U64 oneNW(U64 bb) { return (bb << 7) & ~0x8080808080808080; }

//N (north) move one square
U64 oneN(U64 bb)  { return (bb << 8); }

//NE (northeast) move one square
U64 oneNE(U64 bb) { return (bb << 9) & ~0x0101010101010101; }

//E (east) move one square
U64 oneE(U64 bb)  { return (bb << 1) & ~0x0101010101010101; }

//W (west) move one square
U64 oneW(U64 bb)  { return (bb >> 1) & ~0x8080808080808080; }

//SE (southeast) move one square
U64 oneSE(U64 bb) { return (bb >> 7) & ~0x0101010101010101; }

//S (south) move one square
U64 oneS(U64 bb)  { return (bb >> 8); }

//SW (southwest) move one square
U64 oneSW(U64 bb) { return (bb >> 9) & ~0x8080808080808080; }

//for a pawn's first move
U64 twoN(U64 bb)  { return (bb << 16); }


// *** Knight moves ***

/*
U64 NMoves(U64 knightSquare){
   return(NNE(knightSquare) | NNW(knightSquare) | NEE(knightSquare) | NWW(knightSquare) | 
          SEE(knightSquare) | SWW(knightSquare) | SSE(knightSquare) | SSW(knightSquare));
}
*/

//    NNW           NNE
//
//      NW   N    NE
//NWW                   NEE
//      W   Pawn   E
//SWW                   SEE
//      SW   S    SE
//
//    SSW           SSE

//  ABCDEFGH
//1 XXXXXXXX
//2 XXXXXXXX
//3 XXXXXXXX
//4 XXXXXXXX
//5 XXXXkXXX
//6 XXXXXXXX
//7 XXXXXXXX
//8 XXXXXXXX
U64 NNE(U64 bb) { return (bb << 17) & ~0x0101010101010101; } //not A file

U64 NNW(U64 bb) { return (bb << 15) & ~0x8080808080808080; } //not H files

U64 NEE(U64 bb) { return (bb << 10) & ~0x0303030303030303; } //not AB files

U64 NWW(U64 bb) { return (bb << 6)  & ~0xC0C0C0C0C0C0C0C0; } //not GH files

U64 SEE(U64 bb) { return (bb >> 6)  & ~0x0303030303030303; } //not AB files

U64 SWW(U64 bb) { return (bb >> 10) & ~0xC0C0C0C0C0C0C0C0; } //not GH files

U64 SSE(U64 bb) { return (bb >> 15) & ~0x0101010101010101; } //not A files

U64 SSW(U64 bb) { return (bb >> 17) & ~0x8080808080808080; } //not H files

U64 NMovesAll(U64 knightSquare){
   return(NNE(knightSquare) | NNW(knightSquare) | NEE(knightSquare) | NWW(knightSquare) |
          SEE(knightSquare) | SWW(knightSquare) | SSE(knightSquare) | SSW(knightSquare));
}

