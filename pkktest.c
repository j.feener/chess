//put this inside main function in chess.c

{

//XXX find all the squares the white pawns can move to:
   U64 empty_sqs = FindEmptySquares(the_board);

   //find the white pawn moves:
   U64 singlewPMoves = wPSingleMove(the_board.bitboard[wP], empty_sqs);
   U64 dblwPMoves = wPDblMove(the_board.bitboard[wP], empty_sqs);

   //print them out:
   printf("Single white pawn moves:\n");
   PrintBitBoard(singlewPMoves);
   printf("\n");

   printf("Double white pawn moves:\n");
   PrintBitBoard(dblwPMoves);
   printf("\n");

   //find the black pawn moves:
   U64 singlebPMoves = bPSingleMove(the_board.bitboard[bP], empty_sqs);
   U64 dblbPMoves = bPDblMove(the_board.bitboard[bP], empty_sqs);

   //print them out:
   printf("Single black pawn moves:\n");
   PrintBitBoard(singlebPMoves);
   printf("\n");

   printf("Double black pawn moves:\n");
   PrintBitBoard(dblbPMoves);
   printf("\n");

//XXX find all the squares which have pawns that can move:
   //find the squares with white pawns:
   U64 wPthatCanBeSingleMoved = wPAble2SingleMove(the_board.bitboard[wP], empty_sqs);
   U64 wPthatCanBeDblMoved = wPAble2DblMove(the_board.bitboard[wP], empty_sqs);

   //print them out:
   printf("Single white pawn moves:\n");
   PrintBitBoard(wPthatCanBeSingleMoved);
   printf("\n");

   printf("Double white pawn moves:\n");
   PrintBitBoard(wPthatCanBeDblMoved);
   printf("\n");

   //find the squares with black pawns:
   U64 bPthatCanBeSingleMoved = bPAble2SingleMove(the_board.bitboard[bP], empty_sqs);
   U64 bPthatCanBeDblMoved = bPAble2DblMove(the_board.bitboard[bP], empty_sqs);

   //print them out:
   printf("Single black pawn moves:\n");
   PrintBitBoard(bPthatCanBeSingleMoved);
   printf("\n");

   printf("Double black pawn moves:\n");
   PrintBitBoard(bPthatCanBeDblMoved);
   printf("\n");


//XXX find King Moves:
   
   U64 wKm = KMoves(the_board.bitboard[wK]);
   U64 bKm = KMoves(the_board.bitboard[bK]);

   PrintBitBoard(wKm);
   PrintBitBoard(bKm);

//XXX King and Knight moves from d4:
   U64 d4 = 0x0000000008000000;

   printf("d4:\n");
   PrintBitBoard(d4);

   U64 Km = KMoves(d4);

   printf("King moves from d4:\n");
   PrintBitBoard(Km);

   U64 Nm = NMoves(d4);

   printf("Knight moves from d4:\n");
   PrintBitBoard(Nm);

}

