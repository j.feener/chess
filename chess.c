/*Josh Feener's Chess Program 2017
Sources: Bluefever Software (youtube channel) "Programming A Chess Engine in C"
         Chess programming wiki: https://chessprogramming.wikispaces.com
         Rival Chess (for magic bitboards): http://www.rivalchess.com/magic-bitboards/
*/         

#include "defs.h"
#include <stdlib.h>

int main(void)
{
   BOARD *the_board;
   the_board = malloc(sizeof(BOARD));
   the_board = setboard(the_board); //initialize the board

   struct in_move *current_move;
   current_move = malloc(sizeof(struct in_move));

   struct in_move *comp_move;
   comp_move = malloc(sizeof(struct in_move));   //need to allocate space for struct *!

   struct in_move *block_move[16];
   for(int block_count=0; block_count<16; block_count++){
      block_move[block_count] = malloc(sizeof(struct in_move));
   }

   int goes_first, side_to_move = 0;
   U64 move_bb;

   _Bool isValid = 0, king_in_check, can_block_king;
//   print_num_board();
   print_welcome_message();
   goes_first = get_side_to_move();
   
   if(goes_first == 1){ //computer goes first
      printf("Computer move, thinking...\n");
      isValid = 0;
      while(isValid == 0){
         isValid = computerMove(the_board, comp_move, side_to_move);
      }
      isValid = 0; 
      updateBoard(the_board, comp_move, side_to_move); //XXX this needs to be fixed 
      printf("%c%c%i-%c%i\n", comp_move->piece_type, comp_move->filef, comp_move->rankf, comp_move->filet, comp_move->rankt);     
      side_to_move = switch_turn(side_to_move);
   }   

   while(1) {
      while(!isValid){
         if((current_move = get_move(side_to_move, current_move)) == NULL){
            printf("exiting program\n");
	    return(0);
	 } 

         U64 allPieces = FindOccupiedSquares(the_board);
         U64 friendlyPieces = the_board->bitboard[side_to_move];
         int enemy_side = switch_turn(side_to_move);
         U64 enemyPiecesbb = the_board->bitboard[enemy_side];

         U64 where_piece = the_board->bitboard[current_move->piece_num]; //loads the bitboard of where 
                                                                    //the current type of piece is
         isValid = isValidMove(the_board, current_move, where_piece, allPieces, friendlyPieces, enemyPiecesbb, side_to_move);

         if(!isValid){ printf("move is not valid! %i\n", isValid); } 
      }
      isValid = 0;
      king_in_check = enemyKingInCheck(the_board, current_move, side_to_move);
      if(king_in_check){
         can_block_king = canBlockKing(the_board, current_move, side_to_move);
         if(king_in_check && can_block_king){
            blockKing(the_board, block_move, current_move, side_to_move);
         }
         else if(king_in_check && !can_block_king){
            printf("Good game! You win! 1\n");
            return(0);
            //end the game!
         }
      }

      updateBoard(the_board, current_move, side_to_move);

      side_to_move = switch_turn(side_to_move);

      //XXX do computer move XXX 
      printf("Computer move, thinking...\n");
//      printf("breakpoint 1\n");
      if(king_in_check && can_block_king){
         comp_move = block_move[0]; //make the computer move the first generated block move
      } else {
         while(isValid == 0){
            isValid = computerMove(the_board, comp_move, side_to_move);
//            printf("isValid = %i\n", isValid);
         }
      }
      isValid = 0;

      updateBoard(the_board, comp_move, side_to_move);
      printf("%c%c%i-%c%i\n", comp_move->piece_type, comp_move->filef, comp_move->rankf, comp_move->filet, comp_move->rankt);     
      printf("*******************************************************************\n");
      king_in_check = enemyKingInCheck(the_board, current_move, side_to_move);

      if(king_in_check){
         can_block_king = canBlockKing(the_board, current_move, side_to_move);
         if(king_in_check && can_block_king){
            blockKing(the_board, block_move, current_move, side_to_move);
         }
         else if(king_in_check && !can_block_king){
            printf("Good game! You win! 2\n");
            return(0);
            //end the game!
         }
      }

      side_to_move = switch_turn(side_to_move);
   }
//   move_bb = calculate(current_move, allPiecesbb, friendlyPiecesbb, side_to_move);

   
/*
   move_bb = calculatebb(current_move);

   PrintBitBoard(move_bb);
*/
   //check if end of game

   //generate computer move

/*
   printf("Move from struct move current_move: %c%c%i-%c%i\n", current_move->piece_type, current_move->filef,
                                                               current_move->rankf, current_move->filet, current_move->rankt);

   printf("Square from: %i  Square to: %i\n", current_move->sq_from, current_move->sq_to);
*/
/*
   U64 empty_sqs = FindEmptySquares(the_board);
   U64 allPieces = 0x717299130C44ED42;//FindOccupiedSquares(the_board);
   U64 friendlyPieces = 0x001008100044ED42;
   U64 queenSq = 0x0000000000200000;
   PrintBitBoard(queenSq);
//are U64 rookbb and U64 bishopbb needed in functions?
   U64 qbb = queenMove(F3, allPieces, friendlyPieces);
   printf("%llx\n", qbb);
   PrintBitBoard(qbb);

   struct gen_move moves_to[64];
   for(int i=0; i<64; i++){
      moves_to[i].sq = 0;
   }

   generateMoves(moves_to, current_move->piece_type, qbb);

   printGenMoves(moves_to, current_move->piece_type);
*/
   return 0;
}

void print_welcome_message(){
   printf("\n");
   //print_num_board(); printf("\n");
   printf("Welcome to chess\n");
   printf("Play at your own risk\n");
   printf("Enter q at any time to exit the game\n\n");
   printf("Would you like to be white or black?\n");
   printf("Type 0 for white and 1 for black\n");
}

//*****************************//
//      setboard function      //
//*****************************//

/* Sets the board by 1) initializing the bitboards. 
   2) initializing the Forsyth–Edwards string
   3) setting the number of moves to zero
*/
BOARD *setboard(BOARD *the_board)
{
   the_board->bitboard[w] = 0x000000000000FFFF;
   the_board->bitboard[b] = 0xFFFF000000000000;

   the_board->bitboard[wP] = 0x000000000000FF00;
   the_board->bitboard[bP] = 0x00FF000000000000;

   the_board->bitboard[wN] = 0x0000000000000042;
   the_board->bitboard[bN] = 0x4200000000000000;

   the_board->bitboard[wB] = 0x0000000000000024;
   the_board->bitboard[bB] = 0x2400000000000000;

   the_board->bitboard[wR] = 0x0000000000000081;
   the_board->bitboard[bR] = 0x8100000000000000;

   the_board->bitboard[wQ] = 0x0000000000000008;
   the_board->bitboard[bQ] = 0x0800000000000000;

   the_board->bitboard[wK] = 0x0000000000000010;
   the_board->bitboard[bK] = 0x1000000000000000;

   the_board->bitboard[EMPTY] = 0ULL;

   strcpy(the_board->fen_board, "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"); //Forsyth–Edwards Notation

   the_board->moves = 0; //number of moves is zero

   return the_board;
}

int switch_turn(int turn){
   if(turn == 1){ return(0); }
   if(turn == 0){ return(1); }
}


//returns all the squares that are not occupied by a white or black piece.

U64 FindEmptySquares(BOARD *the_board) { return ~(the_board->bitboard[w] | the_board->bitboard[b]); }

U64 FindOccupiedSquares(BOARD *the_board) {
   return(the_board->bitboard[w] | the_board->bitboard[b] | the_board->bitboard[wP] | the_board->bitboard[bP] |
          the_board->bitboard[wN] | the_board->bitboard[bN] | the_board->bitboard[wB] | the_board->bitboard[bB] |
          the_board->bitboard[wR] | the_board->bitboard[bR] | the_board->bitboard[wQ] | the_board->bitboard[bQ] |
          the_board->bitboard[wK] & the_board->bitboard[bK]);
}

/*
struct move{
   char piece_type, filef, filet;
   int piece_num, sq_from, sq_to, rankf, rankt;
};*/
void generateMoves(int *sq_nums, char piece_type, U64 piece_bb){
   U64 sq = 0;
   int count = 0;
   for(int i=0; i < 64; i++){
      sq = (1ULL << i) & (piece_bb);
      if(sq != 0){
         sq_nums[count++] = i;
      }
      sq = 0;
   }
}

/*
struct gen_move{ //generated move for when calculating
   char piece_type;
   int sq;
}
*/

void printGenMoves(int *sq_nums, char piece_type){
   int sq, rank, i = 0;
   char file;
   U64 board;
   sq = sq_nums[i];
   while(sq != -1){
      printf("sq_nums[%i] = %i\n", i, sq);
      file = findFile(sq);
      rank = findRank(sq);
      printf("%c%c%i\n", piece_type, file, rank);
      board = board | (1ULL << sq);
      i++;
      sq = sq_nums[i];
   }
   PrintBitBoard(board);
}

char findFile(int sq_num){
   int file_num = sq_num%8;
   char file;
   switch(file_num){
      case 0: file = 'a'; break;
      case 1: file = 'b'; break;
      case 2: file = 'c'; break;
      case 3: file = 'd'; break;
      case 4: file = 'e'; break;
      case 5: file = 'f'; break;
      case 6: file = 'g'; break;
      case 7: file = 'h'; break;
   }
   //printf("file: %c\n", file);
   return(file);
}

int findRank(int sq_num){ return(sq_num/8 + 1); }


void PrintBitBoard(U64 bitboard)
{
   unsigned long long shiftMe = 1ULL;
   int rank = 0;
   int file = 0;
   int sq = 0;

   printf("\n");
   for(rank = RANK_8; rank >= RANK_1; rank--)
   {
      for(file = FILE_A; file <= FILE_H; file++)
      {
         sq = FRtoSQ(file,rank);

         if((shiftMe << sq) & bitboard)
            printf("X");
         else
            printf("-");
      }
      printf("\n");
   }
}

