/* Josh Feener's Chess Program
 * Move Input file.
 * This file contains all the functions for the user inputting a move.
 * This program displays the number of each square on the chessboard
 *  and then for a given square, calculates the square's number.
 */


#include <stdio.h>
#include "defs.h"

/******************************************/
/*  get_side_to_move()                    */
/*  User inputs which side will go first  */
/******************************************/

int get_side_to_move(){
    char side[32];
    int side_to_move = -1, stm;
    while(1){
        fgets(side, 30, stdin);  //needs to be a big number
        if(1 == sscanf(side, "%i", &stm)){ side_to_move = stm; }
        if((side_to_move != 0) && (side_to_move != 1)) {  //if incorrect input, ask until correct
            printf("Please enter either 0 for white and 1 for black\n");
	} else { 
	    break;
	}
    }
//    printf("Side to move: %i\n", side_to_move);
    printf("\n"); return(side_to_move);
}

/*******************************/
/*  get_move(int side_to_move) */
/*  User inputs their move     */
/*******************************/

struct in_move* get_move(int side_to_move, struct in_move* current_move){
   char move_string[32];
   int flag = 1;
   while(flag){
      printf("Enter your move:\n"); //Get move input 
      fgets(move_string, 30, stdin);
      if(strcmp(move_string, "q\n") == 0){
	 return(0); //quit game
      }
      if((flag = parse_move(move_string, current_move, side_to_move)) == 1){
	 fprintf(stderr, "Invalid input\n");
      } 
   }
   printf("\n");
   return(current_move);
}

/*******************************/
/*  parse_move()               */
/*  Parses the move input into 
 *  elements for the computer  */
/*******************************/

int parse_move(char* move_string, struct in_move* current_move, int side_to_move){
   char piece, filef, filet;
   int piece_num, rankf, rankt, sq_from = -1, sq_to = -1, flag = 1; //0 = done
   if(5 == sscanf(move_string, "%c%c%i-%c%i", &piece, &filef, &rankf, &filet, &rankt)){
      //printf("%c%c%i-%c%i\n", piece, filef, rankf, filet, rankt); 
      if(((rankf >= 1) && (rankf <= 8)) && ((rankt >= 1) && (rankt <= 8))) {
         sq_from = sq_function(filef, rankf, sq_from); //get sq nums
         sq_to   = sq_function(filet, rankt, sq_to);
         flag = 0;
      }
      piece_num = piece_correct(piece, side_to_move);
      if(piece_num == 0){ flag = 1; }
   } 
   save_current_move(current_move, &piece, &piece_num, &sq_from, &sq_to, &filef, &rankf, &filet, &rankt);
   return(flag);
}

/*********************/
/* save_current_move */
/*********************/

void save_current_move(struct in_move* current_move, char* piece, int* piece_num, 
		int* sq_from, int* sq_to, char* filef, int* rankf, char* filet, int* rankt){
   current_move->piece_type = *piece;
   current_move->piece_num = *piece_num;
   current_move->sq_from = *sq_from;
   current_move->sq_to = *sq_to;
   current_move->filef = *filef;
   current_move->rankf = *rankf;
   current_move->filet = *filet;
   current_move->rankt = *rankt;
}

/*******************************************/
/* piece_correct(char pt, int side_to_move)
 * Finds the piece number                  */
/*******************************************/

int piece_correct(char pt, int side_to_move){
   int piece_type;
   switch(pt){
      case 'K' :
         piece_type = wK + side_to_move; 
         break;
      case 'Q' :
         piece_type = wQ + side_to_move;
         break;
      case 'R' :
         piece_type = wR + side_to_move;
         break;
      case 'B' :
         piece_type = wB + side_to_move;
         break;
      case 'N' :
         piece_type = wN + side_to_move;
         break;
      default  :
         if(pt != 'P'){ return(0); } //XXX why is this?
         piece_type = wP + side_to_move;
   }
   return(piece_type);
}

/*************************************************/
/*  sq_function(char file, int rank, int sq_num) */
/*  Calculates sq_num, which is from 1-64        */
/*************************************************/

int sq_function(char file, int rank, int sq_num){
   switch(file){
      case 'a' :
         sq_num = 0 + 8*(rank - 1);
         break;
      case 'b' :
         sq_num = 1 + 8*(rank - 1);
         break;
      case 'c' :
         sq_num = 2 + 8*(rank - 1);
         break;
      case 'd' :
         sq_num = 3 + 8*(rank - 1);
         break;
      case 'e' :
         sq_num = 4 + 8*(rank - 1);
         break;
      case 'f' :
         sq_num = 5 + 8*(rank - 1);
         break;
      case 'g' :
         sq_num = 6 + 8*(rank - 1);
         break;
      case 'h' :
         sq_num = 7 + 8*(rank - 1);
         break;
      default  : 
         sq_num = -1;
   }
   return(sq_num); 
}

/***********************/
/*  print_num_board()  
 *  Prints out a board */
/***********************/

// This function displays the number of each square on the chessboard
void print_num_board(){
   int i, j, rank = 8;
   //display the number of each square on the chessboard:
   printf("  a  b  c  d  e  f  g  h\n");
   for(i = 56; i >= 0; i -= 8)
   {
      printf("%i ", rank--);
      for(j = i; j < (i+8); j++)
      {
         printf("%2i ", j);
      }
      printf("\n");
   }
}
