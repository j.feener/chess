/*Josh Feener's Chess Program
Started 2-2-2017
*/
#ifndef DEFS_H
#define DEFS_H

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h> //just for the random numbers

#define FRtoSQ(f,r) ( f + 8*(r) )   //just used to print bitboards

/*board representation*/

typedef unsigned long long U64;

typedef struct board{
   U64 bitboard[14];                //14 bitboards to represent pawn-king white and black, plus all white and all black, and 14 = EMPTY
   int8_t board_64[64];             //represents each square in 8 bits
   char fen_board[84];                 //Forsyth–Edwards Notation: Will be initialized to "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1" 
   int moves;                       //counts how many half moves have been made. Will be initialized to 0

   //conditions
   int8_t side_to_move;             //WHITE = 0 , BLACK = 1
   int8_t move_50_rule;             //keeps track of how many moves since last pawn move
   int8_t w_castling_rights;        //
   int8_t b_castling_rights;
   int8_t en_passant;
}BOARD;

struct in_move{ //input move for when user types in move
   char piece_type, filef, filet;
   int piece_num, sq_from, sq_to, rankf, rankt;
};

struct gen_move{ //generated move for when calculating
   char piece_type;
   int sq;
};

enum { w , b , wP , bP , wN , bN , wB , bB , wR , bR , wQ , bQ , wK , bK , EMPTY}; //w=0, bK=13, EMPTY = 14 
enum { FILE_A , FILE_B , FILE_C , FILE_D , FILE_E , FILE_F , FILE_G , FILE_H , FILE_NONE };
enum { RANK_1 , RANK_2 , RANK_3 , RANK_4 , RANK_5 , RANK_6 , RANK_7 , RANK_8 , RANK_NONE };
enum {
   A1 , B1 , C1 , D1 , E1 , F1 , G1 , H1 ,
   A2 , B2 , C2 , D2 , E2 , F2 , G2 , H2 , 
   A3 , B3 , C3 , D3 , E3 , F3 , G3 , H3 , 
   A4 , B4 , C4 , D4 , E4 , F4 , G4 , H4 , 
   A5 , B5 , C5 , D5 , E5 , F5 , G5 , H5 ,
   A6 , B6 , C6 , D6 , E6 , F6 , G6 , H6 , 
   A7 , B7 , C7 , D7 , E7 , F7 , G7 , H7 ,
   A8 , B8 , C8 , D8 , E8 , F8 , G8 , H8 , NO_SQ
};
enum { FALSE , TRUE };


/*Function declarations*/
//chess.c:
void print_welcome_message();
BOARD *setboard(BOARD *the_board);
//BOARD setboard(BOARD the_board);
U64 FindEmptySquares(BOARD *the_board);
U64 FindOccupiedSquares(BOARD *the_board);
int switch_turn(int turn);

void generateMoves(int *sq_nums, char piece_type, U64 piece_bb);
void printGenMoves(int *sq_nums, char piece_type);
char findFile(int sq_num);
int findRank(int sq_num);

//pkk.c:
U64 pawnMove(U64 pawnSq, int sq_num, U64 allPieces, U64 enemyPiecesbb, int side_to_move);
U64 kingMove(U64 kingSq, U64 friendlyPiecesbb);
U64 knightMove(U64 knightSq, U64 friendlyPiecesbb);

U64 wPSingleMove(U64 wpawns, U64 empty);
U64 wPDblMove(U64 wpawns, U64 empty);
U64 bPSingleMove(U64 bpawns, U64 empty);
U64 bPDblMove(U64 bpawns, U64 empty);

U64 wPAble2SingleMove(U64 wpawns, U64 empty_sqs);
U64 wPAble2DblMove(U64 wpawns, U64 empty_sqs);
U64 bPAble2SingleMove(U64 bpawns, U64 empty_sqs);
U64 bPAble2DblMove(U64 bpawns, U64 empty_sqs);

U64 KMovesAll(U64 kingSquare);

U64 oneNW(U64 bb);
U64 oneN(U64 bb);
U64 oneNE(U64 bb);
U64 oneE(U64 bb);
U64 oneW(U64 bb);
U64 oneSE(U64 bb);
U64 oneS(U64 bb);
U64 oneSW(U64 bb);

U64 NMovesAll(U64 knightSquare);

U64 NNE(U64 bb);
U64 NNW(U64 bb);
U64 NEE(U64 bb);
U64 NWW(U64 bb);
U64 SEE(U64 bb);
U64 SWW(U64 bb);
U64 SSE(U64 bb);
U64 SSW(U64 bb);

//brq.c:
U64 rookMove(int sqNum, U64 allPiecesbb, U64 friendlyPiecesbb);
U64 bishopMove(int sqNum, U64 allPiecesbb, U64 friendlyPiecesbb);
U64 queenMove(int sqNum, U64 allPiecesbb, U64 friendlyPiecesbb);
/*U64 rookMove(U64 rookbb, int sqNum, U64 allPiecesbb, U64 friendlyPiecesbb);
U64 bishopMove(U64 bishopbb, int sqNum, U64 allPiecesbb, U64 friendlyPiecesbb);
U64 queenMove(U64 queenbb, int sqNum, U64 allPiecesbb, U64 friendlyPiecesbb);
*/
//move_input.c:
void print_num_board();
int get_side_to_move();
int parse_move(char* move_string, struct in_move* current_move, int side_to_move);
struct in_move *get_move(int side_to_move, struct in_move *current_move);
void save_current_move(struct in_move* current_move, char* piece, int* piece_num, 
	               int* sq_from, int* sq_to, char* filef, int* rankf, char* filet, int* rankt);
int sq_function(char file, int rank, int sq_num);
int piece_correct(char pt, int side_to_move);

//calculate.c:
_Bool computerMove(BOARD *the_board, struct in_move *comp_move, int side_to_move);
void updateBoard(BOARD *the_board, struct in_move *current_move, int side_to_move);
U64 calculate(BOARD *the_board, char piece_type, int sq_from, U64 allPiecesbb, U64 friendlyPiecesbb, U64 enemyPiecesbb, int side_to_move);
_Bool isValidMove(BOARD *the_board, struct in_move *current_move, U64 where_piece, U64 allPiecesbb, U64 friendlyPiecesbb, U64 enemyPiecesbb, int side_to_move);
U64 checkedKingMove(BOARD *the_board, int side_to_move);
U64 calculatebb(int shift);
int number_of_pieces(U64 bb);
int calculateSq(U64 bb);
void calculateSqs(U64 bb, int *sq_nums);
char piece_type_correct(int piece_num);
char randomPiece();

//evaluate.c:
_Bool enemyKingInCheck(BOARD *the_board, struct in_move *current_move, int side_to_move);
_Bool enemyKingStillInCheck(U64 allPieces, BOARD *the_board, struct in_move *current_move, int side_to_move);
_Bool canBlockKing(BOARD *the_board, struct in_move *current_move, int side_to_move);
void blockKing(BOARD *the_board, struct in_move **block_move, struct in_move *current_move, int side_to_move);

//other:
void PrintBitBoard(U64 bitboard);

#endif
