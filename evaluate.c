#include "defs.h"

_Bool enemyKingInCheck(BOARD *the_board, struct in_move *current_move, int side_to_move ){
   U64 allPieces = FindOccupiedSquares(the_board);
   U64 friendlyPieces = the_board->bitboard[side_to_move]; 
   int enemy_side = switch_turn(side_to_move);
   U64 enemyPieces = the_board->bitboard[enemy_side];

   U64 pieces_attacks = calculate(the_board, current_move->piece_type, current_move->sq_to, allPieces, friendlyPieces, enemyPieces, side_to_move); //see if the current piece can attack the opposing side's king

   //print out bitboard of pieces_attacks
   printf("\npieces_attacks: \n");
   PrintBitBoard(pieces_attacks);
   printf("\nthe_board->bitboard[wK + enemy_side]:\n");
   PrintBitBoard(the_board->bitboard[wK]);

   if( (pieces_attacks & the_board->bitboard[wK + enemy_side]) != 0) { return(1); }
   else{ return(0); }
}

_Bool enemyKingStillInCheck(U64 allPieces, BOARD *the_board, struct in_move *current_move, int side_to_move){
   U64 friendlyPieces = the_board->bitboard[side_to_move];
   int enemy_side = switch_turn(side_to_move);
   U64 enemyPieces = the_board->bitboard[enemy_side];

   U64 pieces_attacks = calculate(the_board, current_move->piece_type, current_move->sq_to, allPieces, friendlyPieces, enemyPieces, side_to_move);

   if( (pieces_attacks & the_board->bitboard[wK + enemy_side]) != 0) { return(1); }
   else{ return(0); }
}

_Bool canBlockKing(BOARD *the_board, struct in_move *current_move, int side_to_move){
   U64 allPieces = FindOccupiedSquares(the_board);
   U64 friendlyPieces = the_board->bitboard[side_to_move];

   int enemy_side = switch_turn(side_to_move);
   U64 enemyPiecesbb = the_board->bitboard[enemy_side];

   U64 kingSq = calculatebb(the_board->bitboard[wK + side_to_move]);
   U64 allDefenseMoves = 0;
   for(int piece_num = 2 + side_to_move; piece_num < 12; piece_num = piece_num+2){
      char piece_type = piece_type_correct(piece_num);

      U64 current_sq = the_board->bitboard[piece_num];    //if there is no piece of its kind
      if(number_of_pieces(current_sq) != 0){              //to prevent division by zero
         int sq_nums_from[64];
         for(int i=0; i<64; i++) { sq_nums_from[i] = -1; }
         calculateSqs(current_sq, sq_nums_from);

         int i = 0, sq_from = sq_nums_from[i];
         while(sq_from != -1){
            U64 piece_attacks = calculate(the_board, piece_type, sq_from, allPieces, friendlyPieces, enemyPiecesbb, side_to_move);
            U64 newBoard = allPieces | piece_attacks;
            if(!enemyKingStillInCheck(newBoard, the_board, current_move, side_to_move)){ return(1); }
            i++;
            sq_from = sq_nums_from[i];
         }
      }
   }
   return(0);
}

//this function is used only if the previous function(^) returns a 1
//this function finds if the check made can be blocked, 
void blockKing(BOARD *the_board, struct in_move **block_move, struct in_move *current_move, int side_to_move){
//   struct in_move *block_move[16];
   int block_count = 0;

   U64 allPieces = FindOccupiedSquares(the_board);
   U64 friendlyPieces = the_board->bitboard[side_to_move];

   int enemy_side = switch_turn(side_to_move);
   U64 enemyPiecesbb = the_board->bitboard[enemy_side];

   U64 kingSq = calculatebb(the_board->bitboard[wK + side_to_move]);
   //first put all the squares the enemy pieces are attacking into a bitboard
   U64 allDefenseMoves = 0;
   for(int piece_num = 2 + side_to_move; piece_num < 12; piece_num = piece_num+2){
      char piece_type = piece_type_correct(piece_num);

      U64 current_sq = the_board->bitboard[piece_num];    //if there is no piece of its kind
      if(number_of_pieces(current_sq) != 0){              //to prevent division by zero
         int sq_nums_from[64];
         for(int i=0; i<64; i++) { sq_nums_from[i] = -1; }
         calculateSqs(current_sq, sq_nums_from);

         int i = 0, sq_from = sq_nums_from[i];
         while(sq_from != -1){
            U64 piece_attacks = calculate(the_board, piece_type, sq_from, allPieces, friendlyPieces, enemyPiecesbb, side_to_move);
            if(number_of_pieces(piece_attacks) != 0){
               int sq_nums_to[64];
               for(int j=0; j<64; j++) { sq_nums_to[j] = -1; }
               calculateSqs(piece_attacks, sq_nums_to);

               int j = 0, sq_to = sq_nums_to[j];
               while(sq_to != -1){
                  U64 newBoard = allPieces | (1ULL << sq_to);
                  if(!enemyKingStillInCheck(newBoard, the_board, current_move, side_to_move)){
                     block_move[block_count]->piece_type = piece_type;
                     block_move[block_count]->piece_num = piece_num;
                     block_move[block_count]->sq_from = sq_from;
                     block_move[block_count]->sq_to = sq_to;
                     block_move[block_count]->filef = findFile(sq_from);
                     block_move[block_count]->rankf = findRank(sq_from);
                     block_move[block_count]->filet = findFile(sq_to);
                     block_move[block_count]->rankt = findRank(sq_to);
                     block_count++;
                  }
                  j++;
                  sq_to = sq_nums_to[j];
               }
            }
            i++;
            sq_from = sq_nums_from[i];
         }
      }
   }
   //then for every king move, AND it with the allEnemyAttacks bb to see if it if legal
}
