#include <stdio.h>

int main(void) 
{ 
   char *str;
   int i;

   fprintf(stdout, "Enter a sentence:");
   fgets(str, 10, stdin);
   sscanf(str, "%i", &i); 
   fprintf(stdout, "Your sentence: %s.", i);
}
