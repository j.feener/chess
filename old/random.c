#include <stdio.h>

int main(void) {
   int i;
   int random_number;
   puts("A list of random numbers:");

   for(i=0; i<10; i++)
   {
      random_number = rand() % 100;
      printf("%i\n", random_number);
   }

   return 0;
}
