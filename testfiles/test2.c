/*Josh Feener's Chess Program
  Test program #2
  This program displays the number of each square on the chessboard
  and then for a given square, calculates the square's number.
*/
#include <stdio.h>

char findFile(int sq_num);
int findRank(int sq_num);

int main(void)
{
   char file, string[4];
   int rank = 8, sq_num;
   int i, j;

   //display the number of each square on the chessboard:
   printf("  a  b  c  d  e  f  g  h\n");
   for(i = 56; i >= 0; i -= 8)
   {
      printf("%i ", rank--);
      for(j = i; j < (i+8); j++)
      {
         printf("%2i ", j);
      }
      printf("\n");
   }

   printf("Please enter a square on the chessboard:\n");

   //for the given square, calculate the square's number:
   fgets(string, 4, stdin);
   if(2 == sscanf(string, "%c%i", &file, &rank))
   {
      if((rank  >= 1) && (rank <= 8))
      {
      switch(file){
         case 'a' :
            sq_num = 0 + 8*(rank - 1);
            fprintf(stdout, "square number is: %i\n", sq_num);
            break;
         case 'b' :
            sq_num = 1 + 8*(rank - 1);
            fprintf(stdout, "square number is: %i\n", sq_num);
            break;
         case 'c' :
            sq_num = 2 + 8*(rank - 1);
            fprintf(stdout, "square number is: %i\n", sq_num);
            break;
         case 'd' :
            sq_num = 3 + 8*(rank - 1);
            fprintf(stdout, "square number is: %i\n", sq_num);
            break;
         case 'e' :
            sq_num = 4 + 8*(rank - 1);
            fprintf(stdout, "square number is: %i\n", sq_num);
            break;
         case 'f' :
            sq_num = 5 + 8*(rank - 1);
            fprintf(stdout, "square number is: %i\n", sq_num);
            break;
         case 'g' :
            sq_num = 6 + 8*(rank - 1);
            fprintf(stdout, "square number is: %i\n", sq_num);
            break;
         case 'h' :
            sq_num = 7 + 8*(rank - 1);
            fprintf(stdout, "square number is: %i\n", sq_num);
            break;
         default :
            fprintf(stderr, "Invalid input\n");
         }
      }
      else{
         fprintf(stderr, "Invalid input\n");
      }
   }
   else{
      fprintf(stderr, "Invalid input\n");
   }

   file = findFile(sq_num);
   rank = findRank(sq_num);

   printf("File: %c   Rank: %i\n", file, rank);
}

char findFile(int sq_num){
   int file_num = sq_num%8;
   char file;
   switch(file_num){
      case 0: file = 'a'; break;
      case 1: file = 'b'; break;
      case 2: file = 'c'; break;
      case 3: file = 'd'; break;
      case 4: file = 'e'; break;
      case 5: file = 'f'; break;
      case 6: file = 'g'; break;
      case 7: file = 'h'; break;
   }
   //printf("file: %c\n", file);
   return(file);
}

int findRank(int sq_num){ return(sq_num/8 + 1); }
   //printf("rank: %i\n", rank);
