#include <stdio.h>
#include "defs.h"
#include "magics.h"

void PrintBitBoard(U64 bitboard);

typedef unsigned long long U64;

int main(void)
{
   U64 fileA = 0x0101010101010101;
   U64 fileH = 0x8080808080808080;

   U64 h = 0xa0000;

   
/*
   PrintBitBoard(fileA);
   PrintBitBoard(fileH);
*/
/*
   U64 rank4 = 0x00000000FF000000;
   U64 rank5 = 0x000000FF00000000;

   PrintBitBoard(rank4);
   PrintBitBoard(rank5);
*/
/*   U64 bitboard_w = 0x000000000000FFFF;
   U64 bitboard_b = 0xFFFF000000000000;

   U64 bitboard_wP = 0x000000000000FF00;
   U64 bitboard_bP = 0x00FF000000000000;

   U64 bitboard_wN = 0x0000000000000042;
   U64 bitboard_bN = 0x4200000000000000;

   U64 bitboard_wB = 0x0000000000000024;
   U64 bitboard_bB = 0x2400000000000000;

   U64 bitboard_wR = 0x0000000000000081;
   U64 bitboard_bR = 0x8100000000000000;

   U64 bitboard_wQ = 0x0000000000000008;
   U64 bitboard_bQ = 0x0800000000000000;

   U64 bitboard_wK = 0x0000000000000010;
   U64 bitboard_bK = 0x1000000000000000;

   U64 bitboard_EMPTY = 0ULL;

   PrintBitBoard(bitboard_w);
   PrintBitBoard(bitboard_b);
   PrintBitBoard(bitboard_wP);
   PrintBitBoard(bitboard_bP);
   PrintBitBoard(bitboard_wN);
   PrintBitBoard(bitboard_bN);
   PrintBitBoard(bitboard_wB);
   PrintBitBoard(bitboard_bB);
   PrintBitBoard(bitboard_wR);
   PrintBitBoard(bitboard_bR);
   PrintBitBoard(bitboard_wQ);
   PrintBitBoard(bitboard_bQ);
   PrintBitBoard(bitboard_wK);
   PrintBitBoard(bitboard_bK );
   PrintBitBoard(bitboard_EMPTY);
*/
/*
   for(int i=0; i < 64; i++)
   {
      PrintBitBoard(occupancyMaskRook[i]);
      printf("\n");
   }
*/
   return 0;
}

void PrintBitBoard(U64 bitboard)
{
   unsigned long long shiftMe = 1ULL;
   int rank = 0;
   int file = 0;
   int sq = 0;

   printf("\n");
   for(rank = RANK_8; rank >= RANK_1; rank--)
   {
      for(file = FILE_A; file <= FILE_H; file++)
      {
         sq = FRtoSQ(file,rank);

         if((shiftMe << sq) & bitboard)
            printf("X");
         else
            printf("-");
      }
      printf("\n");
   }
}
