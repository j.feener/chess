//This checks the size of the basic data types for a given system
//lines 12-14 are a bit of code from reddit:
//https://www.reddit.com/r/C_Programming/comments/5w10m0/beginner_how_can_i_determine_how_many_bits_are_in/

#include <stdio.h>
#include <limits.h>

//compile with gcc -o test1 test.c
//don't call .exe test!

int main(void)
{
   printf("%d bits in a byte\n", CHAR_BIT);
   printf("range of int: %d--%d\n", INT_MIN, INT_MAX);
   printf("%zu bits in an int\n", sizeof (int) * CHAR_BIT);
   printf("\n\n");
   printf("size of char: %lu\n", sizeof(char));
   printf("size of short int: %lu\n", sizeof(short));
   printf("size of int: %lu\n", sizeof(int));
   printf("size of long int: %lu\n", sizeof(long));
   printf("size of unsigned long long int: %lu\n", sizeof(unsigned long long));

   return 0;
}
