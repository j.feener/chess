/*Josh Feener's Chess Program
Started 2-2-2017
*/
#ifndef DEFS_H
#define DEFS_H

#include <stdio.h>
#include <string.h>
#include <stdint.h>

#define FRtoSQ(f,r) ( f + 8*(r) )

//board representation

typedef unsigned long long U64;

typedef struct board{
   U64 bitboard[14];                //14 bitboards to represent pawn-king white and black, plus all white and all black, and 14 = EMPTY
   int8_t board_64[64];             //represents each square in 8 bits
   char fen_board[84];                 //Forsyth–Edwards Notation: Will be initialized to "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1" 
   int moves;                       //counts how many half moves have been made. Will be initialized to 0

   //conditions
   int8_t side_to_move;             //WHITE = 0 , BLACK = 1
   int8_t move_50_rule;             //keeps track of how many moves since last pawn move
   int8_t w_castling_rights;        //
   int8_t b_castling_rights;
   int8_t en_passant;
}BOARD;

enum { w , b , wP , bP , wN , bN , wB , bB , wR , bR , wQ , bQ , wK , bK , EMPTY}; //w=0, bK=13, EMPTY = 14 
enum { FILE_A , FILE_B , FILE_C , FILE_D , FILE_E , FILE_F , FILE_G , FILE_H , FILE_NONE };
enum { RANK_1 , RANK_2 , RANK_3 , RANK_4 , RANK_5 , RANK_6 , RANK_7 , RANK_8 , RANK_NONE };
enum {
   A1 , B1 , C1 , D1 , E1 , F1 , G1 , H1 ,
   A2 , B2 , C2 , D2 , E2 , F2 , G2 , H2 , 
   A3 , B3 , C3 , D3 , E3 , F3 , G3 , H3 , 
   A4 , B4 , C4 , D4 , E4 , F4 , G4 , H4 , 
   A5 , B5 , C5 , D5 , E5 , F5 , G5 , H5 ,
   A6 , B6 , C6 , D6 , E6 , F6 , G6 , H6 , 
   A7 , B7 , C7 , D7 , E7 , F7 , G7 , H7 ,
   A8 , B8 , C8 , D8 , E8 , F8 , G8 , H8 , NO_SQ
};
enum { FALSE , TRUE };

#endif
