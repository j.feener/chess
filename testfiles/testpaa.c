#include "defs.h"
#include "pawnAttackArray.h"

void PrintBitBoard(U64 bitboard);

int main(void){
   for(int i=0; i<2; i++){
      for(int j=0; j<48; j++){
         PrintBitBoard(paa[i][j]);
      }
      printf("Black pawns ********************\n");
   }
}

void PrintBitBoard(U64 bitboard)
{
   unsigned long long shiftMe = 1ULL;
   int rank = 0;
   int file = 0;
   int sq = 0;

   printf("\n");
   for(rank = RANK_8; rank >= RANK_1; rank--)
   {
      for(file = FILE_A; file <= FILE_H; file++)
      {
         sq = FRtoSQ(file,rank);

         if((shiftMe << sq) & bitboard)
            printf("X");
         else
            printf("-");
      }
      printf("\n");
   }
}
