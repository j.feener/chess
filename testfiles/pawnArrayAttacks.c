/*Written by Josh Feener  6-2-2017
 *generates the array of bitboards for pawn attacks
 */
#include "defs.h"

void PrintBitBoard(U64 bitboard);

int main(void){
   U64 paa[2][48];

   for(int i=0; i<2; i++){
      for(int j=0; j<48; j++){
         paa[i][j] = 0;
      }
   }

   U64 fileA = 0x0101010101010101;
   U64 fileH = 0x8080808080808080;

   for(int i=8; i<56; i++){
      U64 pawnSq = (1ULL << i);
      if(pawnSq & fileA){
         paa[0][i-8] = (pawnSq << 9);
      }
      else if(pawnSq & fileH){
         paa[0][i-8] = (pawnSq << 7);
      }
      else{
         paa[0][i-8] = ((pawnSq << 9) | (pawnSq << 7));
      }
   }

   for(int i=8; i<56; i++){
      U64 pawnSq = (1ULL << i);
      if(pawnSq & fileA){
         paa[1][i-8] = (pawnSq >> 7);
      }
      else if(pawnSq & fileH){
         paa[1][i-8] = (pawnSq >> 9);
      }
      else{
         paa[1][i-8] = ((pawnSq >> 7) | (pawnSq >> 9));
      }
   }

   FILE *fp;
   fp = fopen("pawnAttackArray.h", "w");
   fprintf(fp, "U64 paa[2][48] = {\n");
   for(int i=0; i<2; i++){
      for(int j=0; j<48; j++){
         fprintf(fp, "0x%llx,\n", paa[i][j]); //needs to be %llx
      }
   }
   fprintf(fp, "};");
   fclose(fp);

/*
   printf("U64 paa[2][48] = {\n");
   for(int i=0; i<2; i++){
      for(int j=0; j<48; j++){
         PrintBitBoard(paa[i][j]);
      }
      printf("Black pawns ********************\n");
   }
   printf("};");
*/
}

void PrintBitBoard(U64 bitboard)
{
   unsigned long long shiftMe = 1ULL;
   int rank = 0;
   int file = 0;
   int sq = 0;

   printf("\n");
   for(rank = RANK_8; rank >= RANK_1; rank--)
   {
      for(file = FILE_A; file <= FILE_H; file++)
      {
         sq = FRtoSQ(file,rank);

         if((shiftMe << sq) & bitboard)
            printf("X");
         else
            printf("-");
      }
      printf("\n");
   }
}
