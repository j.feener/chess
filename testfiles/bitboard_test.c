/*Josh Feener's Chess Program
  This file contains functions that test if the bitboards are working correctly
  by printing them to the screen

prototypes:
*/
#include "defs.h"

void PrintBitBoard(U64 bitboard);
void PrintBitBoards(BOARD the_board);

int main(void)
{
   struct board the_board;
   the_board.bitboard[w] = 0x000000000000FFFF;
   the_board.bitboard[b] = 0xFFFF000000000000;

   the_board.bitboard[wP] = 0x000000000000FF00;
   the_board.bitboard[bP] = 0x00FF000000000000;

   the_board.bitboard[wN] = 0x0000000000000042;
   the_board.bitboard[bN] = 0x4200000000000000;

   the_board.bitboard[wB] = 0x0000000000000024;
   the_board.bitboard[bB] = 0x2400000000000000;

   the_board.bitboard[wR] = 0x0000000000000081;
   the_board.bitboard[bR] = 0x8100000000000000;

   the_board.bitboard[wQ] = 0x0000000000000008;
   the_board.bitboard[bQ] = 0x0800000000000000;

   the_board.bitboard[wK] = 0x0000000000000010;
   the_board.bitboard[bK] = 0x1000000000000000;

   the_board.bitboard[EMPTY] = 0ULL;

   PrintBitBoards(the_board);
/*
   U64 notABfiles = ~0x0303030303030303;
   printf("Not AB files:\n");
   PrintBitBoard(notABfiles);
   U64 notGHfiles = ~0xC0C0C0C0C0C0C0C0;
   printf("Not GH files:\n");
   PrintBitBoard(notGHfiles);
*/
/*   U64 notAfile = ~0x0101010101010101;
   U64 notHfile = ~0x8080808080808080;

   printf("Not AB files:\n");
   PrintBitBoard(notABfiles);
   printf("Not A file:\n");
   PrintBitBoard(notAfile);
   printf("Not H file:\n");
   PrintBitBoard(notHfile);
*/
   return 0;
}

void PrintBitBoard(U64 bitboard)
{
   unsigned long long shiftMe = 1ULL;
   int rank = 0;
   int file = 0;
   int sq = 0;

   printf("\n");
   for(rank = RANK_8; rank >= RANK_1; rank--)
   {
      for(file = FILE_A; file <= FILE_H; file++)
      {
         sq = FRtoSQ(file,rank);

         if((shiftMe << sq) & bitboard)
            printf("X");
         else
            printf("-");
      }
      printf("\n");
   }
}

void PrintBitBoards(BOARD the_board)
{
   printf("White pieces:\n\n");
   PrintBitBoard(the_board.bitboard[w]);
   printf("\n\n");

   printf("Black pieces:\n\n");
   printf("%#llx", the_board.bitboard[b]); //keeping this one because it prints correctly
   PrintBitBoard(the_board.bitboard[b]);
   printf("\n\n");

   printf("White Pawns:\n\n");
   PrintBitBoard(the_board.bitboard[wP]);
   printf("\n\n");

   printf("Black Pawns:\n\n");
   PrintBitBoard(the_board.bitboard[bP]);
   printf("\n\n");

   printf("White kNights:\n\n");
   PrintBitBoard(the_board.bitboard[wN]);
   printf("\n\n");

   printf("Black kNights:\n\n");
   PrintBitBoard(the_board.bitboard[bN]);
   printf("\n\n");

   printf("White Bishops\n\n");
   PrintBitBoard(the_board.bitboard[wB]);
   printf("\n\n");

   printf("Black Bishops\n\n");
   PrintBitBoard(the_board.bitboard[bB]);
   printf("\n\n");

   printf("White Rooks\n\n");
   PrintBitBoard(the_board.bitboard[wR]);
   printf("\n\n");

   printf("Black Rooks\n\n");
   PrintBitBoard(the_board.bitboard[bR]);
   printf("\n\n");

   printf("White Queen\n\n");
   PrintBitBoard(the_board.bitboard[wQ]);
   printf("\n\n");

   printf("Black Queen\n\n");
   PrintBitBoard(the_board.bitboard[bQ]);
   printf("\n\n");

   printf("White King\n\n");
   PrintBitBoard(the_board.bitboard[wK]);
   printf("\n\n");

   printf("Black King\n\n");
   PrintBitBoard(the_board.bitboard[bK]);
   printf("\n\n");

   printf("Empty bitboard:\n\n");
   PrintBitBoard(the_board.bitboard[EMPTY]);
   printf("\n\n");
}

